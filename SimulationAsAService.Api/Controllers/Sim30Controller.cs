using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Text;
using System.IO;
using System.Diagnostics;

namespace SimulationAsAService.Api.Controllers
{
    [Route("api/[controller]")]
    public class Sim30Controller : Controller
    {
        private static string _appPath = Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location);

        private static string _exeDirectory = Path.Combine(_appPath, "Executables");
        private static string _as30Path = Path.Combine(_exeDirectory, "as30.exe");
        private static string _sim30Path = Path.Combine(_exeDirectory, "sim30.exe");

        private static string _srcDirectory = Path.Combine(_appPath, "SrcTemp");

        private static List<SessionExecute> _sessionsExecute = new List<SessionExecute>();

        [HttpGet("src")]
        public IActionResult GetSourceCode(string sid, string typ)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(sid) || string.IsNullOrWhiteSpace(typ))
                    return BadRequest();

                string srcPath = GetFilePath(_srcDirectory, sid, typ);
                if (System.IO.File.Exists(srcPath))
                {
                    var result = System.IO.File.ReadAllText(srcPath);
                    return Content(result);
                }
            }
            catch (Exception)
            {

            }

            return NoContent();
        }

        [HttpGet("execute")]
        public IActionResult Execute(string sid, string command = "")
        {
            try
            {
                if (string.IsNullOrWhiteSpace(sid))
                    return BadRequest();

                var sessionExecute = _sessionsExecute.FirstOrDefault(m => m.Sid == sid);
                if (sessionExecute == null)
                {
                    sessionExecute = new SessionExecute() { Sid = sid };
                    _sessionsExecute.Add(sessionExecute);
                }

                command = command.ToLower();

                if (command.Contains("q"))
                {
                    _sessionsExecute.Remove(sessionExecute);
                    return Content("Reset the execution." + Environment.NewLine + "======================================");
                }

                if (!string.IsNullOrEmpty(command))
                    sessionExecute.Commands.Add(command);

                string srcFileName = sid + ".obj";

                var processStartInfo = new ProcessStartInfo(_sim30Path)
                {
                    WorkingDirectory = _srcDirectory,
                    Arguments = srcFileName,
                    UseShellExecute = false,
                    CreateNoWindow = true,
                    RedirectStandardInput = true,
                    RedirectStandardOutput = true,
                    RedirectStandardError = true
                };

                var result = new List<string>();

                using (var process = new Process())
                {
                    process.StartInfo = processStartInfo;
                    process.Start();

                    process.StandardInput.WriteLine(string.Join(" ", sessionExecute.Commands) + " q");

                    while (!process.StandardOutput.EndOfStream)
                    {
                        result.Add(process.StandardOutput.ReadLine());
                    }
                }

                int currentCommandLine = result.Count;
                result.RemoveRange(0, sessionExecute.LatestRowOutput);

                sessionExecute.LatestRowOutput = currentCommandLine;

                if (command.Contains("g"))
                {
                    _sessionsExecute.Remove(sessionExecute);
                }

                return Content(string.Join(Environment.NewLine, result));
            }
            catch (Exception ex)
            {
                return Content(ex.Message);
            }
        }

        [HttpPost("Compile")]
        public IActionResult Compile([FromBody]SimRequest simRequest)
        {
            try
            {
                if (!System.IO.File.Exists(_as30Path))
                    return NotFound("as30.exe");

                string sid = Guid.NewGuid().ToString();
                string srcFileName = sid + ".asm";
                string srcPath = GetFilePath(_srcDirectory, sid, "asm");

                if (!Directory.Exists(_srcDirectory))
                    Directory.CreateDirectory(_srcDirectory);

                System.IO.File.WriteAllLines(srcPath, simRequest.SourceCode.Split(new string[] { "\r\n", "\n" }, StringSplitOptions.None), new UTF8Encoding(false));

                var compileAsToObjResult = CompileAsm(srcFileName);

                return Json(new { Sid = sid, Message = compileAsToObjResult, Compiled = string.IsNullOrEmpty(compileAsToObjResult) });
            }
            catch (Exception ex)
            {
                return Content(ex.Message);
            }
        }

        private string CompileAsm(string srcFileName)
        {
            string result = string.Empty;

            try
            {
                var processStartInfo = new ProcessStartInfo(_as30Path)
                {
                    WorkingDirectory = _srcDirectory,
                    Arguments = srcFileName,
                    UseShellExecute = false,
                    CreateNoWindow = true,
                    RedirectStandardOutput = true
                };

                using (var process = new Process())
                {
                    process.StartInfo = processStartInfo;
                    process.Start();
                    process.WaitForExit();
                    result = process.StandardOutput.ReadToEnd();
                }
            }
            catch (Exception ex)
            {
                // Log error.
            }

            return result;
        }

        private string GetFilePath(string directory, string fileName, string fileExtension)
        {
            string srcFileName = fileName + "." + fileExtension;
            return Path.Combine(directory, srcFileName);
        }
    }

    public class SimRequest
    {
        public string SourceCode { get; set; }
    }

    internal class SessionExecute
    {
        public string Sid { get; set; }
        public List<string> Commands { get; set; }
        public int LatestRowOutput { get; set; }

        public SessionExecute()
        {
            this.Commands = new List<string>();
            LatestRowOutput = 0;
        }
    }
}
